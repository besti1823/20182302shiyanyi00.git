public class LinkedGraph
{
    private int r;
    private LinkedGraph a;
    private LinkedGraph b;
    public LinkedGraph(LinkedGraph b,int r)
    {
        this.r = r;
        this.b = b;
        a = null;
    }
    public LinkedGraph(LinkedGraph peak,int right, LinkedGraph next)
    {
        this.r = r;
        this.a = next;
        this.b = peak;
    }
    public int getr()
    {
        return r;
    }
    public void setr(int r)
    {
        this.r=r;
    }
    public LinkedGraph geta()
    {
        return a;
    }
    public void setNext(LinkedGraph a)
    {
        this.a = a;
    }
    public LinkedGraph getb()
    {
        return b;
    }
    public void setb(LinkedGraph b)
    {
        this.b = b;
    }
}