import java.util.Arrays;
import java.util.Stack;
public class ArrayStack<T> extends Stack<T> {
    private final int DEFAULT_CAPACITY = 100;
    private int count;
    private T[] stack;
    // 使用默认容量创建一个空栈
    public ArrayStack()
    {
        count = 0;
        stack = (T[]) (new Object[DEFAULT_CAPACITY]);
    }
    // 使用指定容量创建一个空栈，参数initialCapacity表示的是指定容量
    public ArrayStack (int initialCapacity)
    {
        count= 0;
        stack = (T[]) (new Object[initialCapacity]);
    }
    public T push (T element)
    {
        if (size() == stack.length)
        {
            expandCapacity();
        }
        stack[count] = element;
        count++;
        return element;
    }
    private void expandCapacity()
    {
        stack = Arrays.copyOf(stack, stack.length * 2);
    }
    public int size() {
        return count;
    }
    public T pop()
    {
        if (isEmpty())
            System.out.println("Empty");
        count--;
        T result = stack[count];
        stack[count] = null;
        return  result;
    }
    public boolean isEmpty() {
        if(stack[0] == null)
        {
            return true;
        }
        return false;
    }
    public T peek()
    {
        if (isEmpty())
            System.out.println("Empty");
        return stack[count- 1];
    }
    public String toString()
    {
        String a =null;
        for (int x = 0;x <count; x++)
        {
            a=a+stack[x] + " ";
        }
        return a;
    }
}