package shiyanjiutu;
import java.util.Scanner;
public class Graph {
    int vertex;
    int edge;
    Vertex[] verArray;
    public Graph() {
        Scanner scan = new Scanner(System.in);
        System.out.println("-----------------------");
        System.out.println("选择要生成的图的种类");
        System.out.println("1.无向图");
        System.out.println("2.有向图");
        System.out.println("-----------------------");
        System.out.println("请输入你的需求编号：");
        int choose = scan.nextInt();
        System.out.println("请输入图的顶点个数:");
        vertex = scan.nextInt();
        System.out.println("请输入图的边数：");
        edge = scan.nextInt();
        verArray = new Vertex[vertex];
        System.out.println("请输入节点的名称:");
        for (int i=0;i<vertex;i++)
        {
            Vertex vertexmem = new Vertex();
            vertexmem.verName = scan.next();
            vertexmem.edgeLink = null;
            verArray[i] = vertexmem;
        }
        System.out.println("请依次输入边的头节点、尾节点：");
        for (int i=0;i<edge;i++)
        {
            String preName = scan.next();
            String nextName = scan.next();
            Vertex preV = getVertex(preName);
            Vertex nextV = getVertex(nextName);
            Edge edge = new Edge();
            edge.tailName = nextName;
            edge.broEdge = preV.edgeLink;
            preV.edgeLink = edge;
            if(choose==1){
                Edge edgeelse = new Edge();
                edgeelse.tailName = preName;
                edgeelse.broEdge  = nextV.edgeLink;
                nextV.edgeLink = edgeelse;}
        }
    }
    public Vertex getVertex(String verName){
        for (int i=0;i<vertex;i++){
            if (verArray[i].verName.equals(verName))
                return verArray[i];
        }
        return null;
    }
}
