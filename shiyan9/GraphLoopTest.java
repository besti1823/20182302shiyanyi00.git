package shiyanjiutu;

import java.util.*;
public class GraphLoopTest {
    private Map<String, List<String>> graph = new HashMap<String, List<String>>();
    public void initGraphData() {
        graph.put("1", Arrays.asList("2", "3"));
        graph.put("2", Arrays.asList("1", "4"));
        graph.put("3", Arrays.asList("1", "5"));
        graph.put("4", Arrays.asList("2"));
        graph.put("5", Arrays.asList("3"));
    }
    private Queue<String> queue = new LinkedList<String>();
    private Map<String, Boolean> status = new HashMap<String, Boolean>();
    public void BFSSearch(String startPoint) {
        queue.add(startPoint);
        status.put(startPoint, false);
        bfsLoop();
    }
    private void bfsLoop() {
        String currentQueueHeader = queue.poll();
        status.put(currentQueueHeader, true);
        System.out.println(currentQueueHeader);
        List<String> neighborPoints = graph.get(currentQueueHeader);
        for (String poinit : neighborPoints) {
            if (!status.getOrDefault(poinit, false)) {
                if (queue.contains(poinit)) continue;
                queue.add(poinit);
                status.put(poinit, false);
            }
        }
        if (!queue.isEmpty()) {
            bfsLoop();
        }
    }
    private Stack<String> stack = new Stack<String>();
    public void DFSSearch(String startPoint) {
        stack.push(startPoint);
        status.put(startPoint, true);
        dfsLoop();
    }
    private void dfsLoop() {
        if(stack.empty()){
            return;
        }
        String stackTopPoint = stack.peek();
        List<String> neighborPoints = graph.get(stackTopPoint);
        for (String point : neighborPoints) {
            if (!status.getOrDefault(point, false)) {
                stack.push(point);
                status.put(point, true);
                dfsLoop();
            }
        }
        String popPoint =  stack.pop();
        System.out.println(popPoint);
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("-----------------------");
        System.out.println("选择要生成的图的种类");
        System.out.println("1.深度优先遍历");
        System.out.println("2.广度优先遍历");
        System.out.println("-----------------------");
        System.out.println("请输入你的需求编号：");
        int choose = scan.nextInt();
        GraphLoopTest test = new GraphLoopTest();
        test.initGraphData();
        if(choose==1){
            System.out.println("深度优先遍历： ");
            test.DFSSearch("1");}
        if(choose==2){
            System.out.println("广度优先遍历 ：");
            test.BFSSearch("1");}
    }
}