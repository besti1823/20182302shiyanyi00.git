import java.io.*;
import java.util.Scanner;

public class Third {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("F:\\java\\yunbankezuoyeceshi", "Third.txt");
        //File file = new File("HelloWorld.txt");
//      File file1 = new File("C:\\Users\\besti\\Desktop\\FileTest\\Test\\Test");
//      file1.mkdir();
//      file1.mkdirs();
        if (!file.exists()) {
            file.createNewFile();
        }
        int num1,num2,num3,num6,num4,num5;
        String content = "";
        Scanner scan = new Scanner(System.in);
        Writer writer2 = new FileWriter(file);
        System.out.println("请输入第一个复数实数部分");
        num1 = scan.nextInt();
        System.out.println("请输入第一个复数虚数部分");
        num2 = scan.nextInt();
        System.out.println("请输入第二个复数实数部分");
        num3 = scan.nextInt();
        System.out.println("请输入第二个复数虚数部分");
        num4 = scan.nextInt();
        num5=num1+num3;
        num6=num2+num4;
        writer2.write("第一个复数：");
        writer2.flush();
        writer2.append(num1+"+"+num2+"i");
        writer2.flush();
        writer2.write("第二个复数：");
        writer2.flush();
        writer2.append(num3+"+"+num4+"i");
        writer2.flush();
        writer2.write("结果：");
        writer2.flush();
        writer2.append(num5+"+"+num6+"i");
        writer2.flush();
        Reader reader2 = new FileReader(file);
        while(reader2.ready()){
            BufferedReader bufferedReader = new BufferedReader(reader2);
            System.out.println("\n下面是用BufferedReader读出的数据：");
            while ((content =bufferedReader.readLine())!= null) {
                System.out.println(content);
            }
        }

    }
}
