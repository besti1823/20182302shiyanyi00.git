import java.io.*;
/**
 * Created by besti on 2018/5/6.
 */
public class First {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("F:\\java\\yunbankezuoyeceshi", "First.txt");
        //File file = new File("HelloWorld.txt");
//        File file1 = new File("C:\\Users\\besti\\Desktop\\FileTest\\Test\\Test");
//        file1.mkdir();
//        file1.mkdirs();
        String content = "";
        if (!file.exists()) {
            file.createNewFile();
        }
        // file.delete();
        //（2）文件读写
        //第一种：字节流读写，先写后读
        Writer writer2 = new FileWriter(file);
        writer2.write("Hello, I/O Operataion!这是利用Writer写入文件的内容");
        writer2.flush();
        writer2.append("Hello,World");
        writer2.flush();
        Reader reader2 = new FileReader(file);
        System.out.println("下面是用Reader读出的数据：");
        while(reader2.ready()){
            System.out.print((char) reader2.read()+ "  ");
        }
    }
}