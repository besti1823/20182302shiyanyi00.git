public interface StackADT2<T> {
        public void NumberInsert(int where, int num);
        public void WhereDelete(int where);
        public boolean isEmpty();
        public int size();
        public T pop();
        public void push(T element);
        public String toString();
    }