import java.util.StringTokenizer;

public class LinkedStack3<T> implements StackADT3<T>
{
    private final int DEFAULT=100;
    private LinnearNode3 Head;
    private int count;
    private String[] Array,temp,stack;
    @Override
    public String toString() {
        String result = "";
        System.out.println("栈中的元素是：" + "\t");

        for(LinnearNode3 current = this.Head; current != null; current = current.getNext()) {
            result = result + current.getElement().toString() + "\t";
        }
        return result;
    }
    public String XuanzeSorting(String s) {
        String result="";
        StringTokenizer stringTokenizer = new StringTokenizer(s);
        int [] score = new int[stringTokenizer.countTokens()];
        for (int i =0;stringTokenizer.hasMoreTokens();i++){
            score[i]=Integer.parseInt(stringTokenizer.nextToken());
        }
        for(int i =0;i < score.length - 1;i++){
            for(int j = i;j <  score.length - 1;j++){
                if(score[i] < score[j+1]) {
                    int k=score[i];
                    score[i]=score[j+1];
                    score[j+1]=k;
                }
            }
        }
        for (int i =0 ;i<score.length;i++){
            result+= score[i]+ " ";
        }
        return result ;
    }
    public T pop() throws EmptyCollectionException2 {
        if (this.isEmpty()) {
            throw new EmptyCollectionException2("Stack");
        } else {
            T result = (T) this.Head.getElement();
            this.Head = this.Head.getNext();
            this.count--;
            return result;
        }
    }
    @Override
    public void push(T element) {
        LinnearNode3 temp = new LinnearNode3(element);
        temp.setNext(this.Head);
        this.Head = temp;
        this.count++;
    }
    @Override
    public void NumberInsert(int where, int num) {
        LinnearNode3 node1,node2;
        LinnearNode3 temp = new LinnearNode3(num);

        if (where==0){
            temp.next = Head;
            Head =temp;
        }
        else {
            node1=Head;
            node2=Head.next;

            for (int i =0; i<where-1;i++){
                node1=node1.next;
                node2=node2.next;
            }
            if (node2!=null){
                temp.next=node2;
                node1.next=temp;
            }
            else {
                node1.next=temp;
            }
            count++;
        }
    }
    @Override
    public void WhereDelete(int where) {
        LinnearNode3 node1=Head,node2=null;
        int i =0;
        if (node1==null)
            System.out.println("链表中无元素！");
        else
            node2 = node1.next;
        while(node1.next!=null&&node2.next!=null&&i<where-1){
            node1=node1.next;
            node2=node2.next;
            i++;
        }

        node1.next = node2.next;
        count--;
    }
    @Override
    public boolean isEmpty() {
        return this.count == 0;
    }
    @Override
    public int size() {
        return count;
    }
}