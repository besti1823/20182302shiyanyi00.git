public class LinnearNode2<T> {
    public LinnearNode2 next;
    private T element;
    public LinnearNode2(){
        next=null;
        element=null;
    }
    public LinnearNode2(T elem)
    {
        next = null;
        element = elem;
    }
    public LinnearNode2<T> getNext(){
        return  next;
    }
    public void setNext(LinnearNode2 node){
        next = node;
    }
    public T getElement(){
        return element;
    }
    public void setElement(T elem) {
        element=elem;
    }
}