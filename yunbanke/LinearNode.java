public class LinearNode<T> {
    protected LinearNode<T> next;
    private int  element;
    public LinearNode(int element) {
        this.element = element;
        this.next=null;
    }
    public LinearNode(){
        this.next=null;
    }
    public void setNext(LinearNode<T> next) {
        this.next = next;
    }
    public void setElement(int element) {
        this.element = element;
    }
    public LinearNode<T> getNext() {
        return next;
    }
    public int getElement() {
        return element;
    }
}