public class LinearList<T> {
    private LinearNode<T> head;
    private LinearNode<T> temp;
    private int count=0;
    public int getCount() {
        return count;
    }
    public LinearList() {
        head=null;
    }
    public LinearNode<T> getHead(){
        return head;
    }
    public void Add(LinearNode<T> a){
        if(head==null){
            head=a;
        }
        else {
            temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = a;
        }
    }
    public void SubJ(int b){
        temp=head;
        while(temp.next.getElement()!=b){
            temp=temp.next;
        }
        temp.next=temp.next.next;
    }
    public void AddJ(int b,int c){
        temp=head;
        while(temp.getElement()!=b){
            temp=temp.next;
        }
        LinearNode<T> a=new LinearNode<T>(c);
        a.next=temp.next;
        temp.next=a;
    }
    public void AddH(int ele1){
        LinearNode<T> a=new LinearNode<T>(ele1);
        a.next=head;
        head=a;
    }
    public String toString(){
        LinearNode<T> temp1=head;
        String result="";
        while(temp1.next!=null){
            result+=temp1.getElement()+"\n";
            temp1=temp1.next;
        }
        result+=temp.getElement();
        return result;
    }
    public int getnLengChong(){
        if(head==null)
            return 0;
        int nLengChong=1;
        LinearNode<T> temp1=head;
        while(temp1.next!=null){
            nLengChong++;
            temp1=temp1.next;
        }
        return nLengChong;
    }
    public void setHead(LinearNode<T> head) {
        this.head = head;
    }
    public void Sort(){
        temp=head;
        int k;
        LinearNode<T> temp1=null;
        while(temp.next!=null){
            temp1=temp.next;
            while(temp1.next!=null){
                if(temp1.getElement()>temp.getElement()){
                    k=temp1.getElement();
                    temp1.setElement(temp.getElement());
                    temp.setElement(k);
                }
                temp1=temp1.next;
            }
            System.out.println("当前链表元素个数为："+this.getnLengChong()+"元素："+this.toString());
            temp=temp.next;
        }
    }
    public LinearNode<T> Count(int index){
        temp=head;
        for(int i=1;i<index;i++){
            temp=temp.next;
        }
        return temp;
    }
}