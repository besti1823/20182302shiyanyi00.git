import java.util.Scanner;

public class Erchatree{
    private Erchatree Left;
    private Erchatree Right;
    private Erchatree parent;
    private Integer data;
    private int result=0;
    private int j=0;
    public Erchatree(Erchatree LeftChild,Erchatree RightChild,Integer data){
        super();
        this.Left= LeftChild;
        this.Right=RightChild;
        this.data=data;
    }
    public Erchatree(Integer data){
        this(null,null,data);
    }
    public Erchatree(){
        super();
    }
    public boolean insert(Erchatree tree,Integer item){
        if(item<tree.data){
            if(tree.Left==null){
                Erchatree temp = new Erchatree(item);
                tree.Left = temp;
                temp.parent = tree;
                return true;
            }
            else{insert(tree.Left,item);
            }
        }else if(item>tree.data){
            if(tree.Right==null){
                Erchatree temp = new Erchatree(item);
                tree.Right = temp;
                return true;
            }else{
                insert(tree.Right,item);
            }
        }return false;
    }
    public void inorder(Erchatree tree){
        if(tree !=null){
            inorder(tree.Left);
            visit(tree.data);
            inorder(tree.Right);}
    }
    private void visit(Integer data){
        System.out.println(data + "");
        int num =0;
        Scanner input=new Scanner(System.in);
        for(;j<1;j++)
        {
            System.out.println("请输入你要查找的数：");
            num = input.nextInt();
        }
        result =result+1;
        if(data==num){
            System.out.println("你输入的数字存在，在数组中的位置是第："+(result)+"个");
        }
    }

    public static void main(String[] args){
        int[]a={19,14,23,1,68,20,84,27,55,11,10,79};
        Erchatree tree = new Erchatree(a[0]);
        for(int i: a){
            tree.insert(tree,i);
        }tree.inorder(tree);
    }
}

