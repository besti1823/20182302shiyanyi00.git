import junit.framework.TestCase;

public class Searching2Test extends TestCase {
        Integer[] a={2,0,1,8,2,3,0,2};
        Integer[] b={2,0,3,2,8,1,0,2};
        Integer[] c={2,0,3};
        Integer[] d={2,0,1};
        Integer[] e={2,0,4};
        public void testNormalLinearSearch(){
            assertEquals(true,Searching2.linearSearch(a,0,a.length,2));
        }
        public void testabNormalLinearSearch(){
            assertEquals(false,Searching2.linearSearch(a,0,a.length,8));
        }
        public void testboundaryLinearSearch(){
            assertEquals(true,Searching2.linearSearch(a,3,a.length,2));
        }
        public void testNormalLinearSearch1(){
        assertEquals(true,Searching2.linearSearch(b,0,b.length,2));
        }
        public void testabNormalLinearSearch1(){
        assertEquals(false,Searching2.linearSearch(b,0,b.length,8));
        }
        public void testboundaryLinearSearch1(){
        assertEquals(true,Searching2.linearSearch(b,3,b.length,2));
        }
    public void testNormalLinearSearch3(){
        assertEquals(true,Searching2.linearSearch(c,0,c.length,2));
    }
    public void testabNormalLinearSearch3(){
        assertEquals(false,Searching2.linearSearch(c,0,c.length,0));
    }
    public void testboundaryLinearSearch3(){
        assertEquals(true,Searching2.linearSearch(c,3,c.length,2));
    }
    public void testNormalLinearSearch4(){
        assertEquals(true,Searching2.linearSearch(d,0,d.length,2));
    }
    public void testabNormalLinearSearch4(){
        assertEquals(false,Searching2.linearSearch(d,0,d.length,0));
    }
    public void testboundaryLinearSearch4(){
        assertEquals(true,Searching2.linearSearch(d,3,d.length,2));
    }
    public void testNormalLinearSearch5(){
        assertEquals(true,Searching2.linearSearch(e,0,e.length,2));
    }
    public void testabNormalLinearSearch5(){
        assertEquals(false,Searching2.linearSearch(e,0,e.length,0));
    }
    public void testboundaryLinearSearch5(){
        assertEquals(true,Searching2.linearSearch(e,3,e.length,2));
    }
    }
