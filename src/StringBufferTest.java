import junit.framework.TestCase;
import org.junit.Test;
public class StringBufferTest extends TestCase{
    StringBuffer a=new StringBuffer("abcefghijklm");
    StringBuffer b=new StringBuffer("abcdefghijklmnopqresuvwx");
    StringBuffer c=new StringBuffer("abcdefghijklmnopqrstuvwxyzbutterflys");
    @Test
    public void testcharAt () throws Exception {
        assertEquals('a',a.charAt(0));
        assertEquals('f',b.charAt(5));
        assertEquals('l',c.charAt(11));
    }
    @Test
    public void testcapacity () throws Exception {
        assertEquals(28,a.capacity());
        assertEquals(40,b.capacity());
        assertEquals(52,c.capacity());
    }
    @Test
    public void testlength () throws Exception {
        assertEquals(12,a.length());
        assertEquals(24,b.length());
        assertEquals(36,c.length());
    }
    @Test
    public void testindexOf () throws Exception {
        assertEquals(0,a.indexOf("abc"));
        assertEquals(6,b.indexOf("ghi"));
        assertEquals(10,c.indexOf("klm"));
    }
}