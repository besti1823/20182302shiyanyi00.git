import org.junit.Assert;
import org.junit.Test;
import junit.framework.TestCase;

public class MyUtilTest {
    @Test
    public void testNormal() {
        Assert.assertEquals("不及格", Myutil.percentage2fivegrade(55));
        Assert.assertEquals("及格", Myutil.percentage2fivegrade(65));
        Assert.assertEquals("中等", Myutil.percentage2fivegrade(75));
        Assert.assertEquals("良好", Myutil.percentage2fivegrade(85));
        Assert.assertEquals("优秀", Myutil.percentage2fivegrade(95));
    }
    @Test
    public void testExptions() {
        //测试出错情况
        if(Myutil.percentage2fivegrade(-10) != "错误")
            System.out.println("test failed 1!");
        else if(Myutil.percentage2fivegrade(115) != "错误")
            System.out.println("test failed 2!");
        else
            System.out.println("test passed!");
    }
    @Test
    public void testBoundary() {
        //测试边界情况
        if(Myutil.percentage2fivegrade(0) != "不及格")
            System.out.println("test failed 1!");
        else if(Myutil.percentage2fivegrade(60) != "及格")
            System.out.println("test failed 2!");
        else if(Myutil.percentage2fivegrade(70) != "中等")
            System.out.println("test failed 3!");
        else if(Myutil.percentage2fivegrade(80) != "良好")
            System.out.println("test failed 4!");
        else if(Myutil.percentage2fivegrade(90) != "优秀")
            System.out.println("test failed 5!");
        else if(Myutil.percentage2fivegrade(100) != "优秀")
            System.out.println("test failed 6!");
        else
            System.out.println("texst passed!");
    }
}