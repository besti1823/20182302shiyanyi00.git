import java.util.Objects;

public class Complex {
    private double RealPart;
    private double ImagePart;
    public Complex(double r, double i) {
        RealPart = r;
        ImagePart = i;
    }

    public double getRealPart(double r) {
        return r;
    }

    public double getImagePart(double i) {
        return i;
    }

    public Complex ComplexAdd(Complex a){
        return new Complex(RealPart +a.RealPart,ImagePart+a.ImagePart);
    }
    public Complex ComplexSub(Complex a){
        return new Complex(RealPart -a.RealPart,ImagePart-a.ImagePart);
    }
    public Complex ComplexMulti(Complex a){
        return new Complex(RealPart * a.RealPart -  ImagePart* a.ImagePart, RealPart * a.ImagePart + ImagePart * a.RealPart);
    }
    public Complex ComplexDiv(Complex a) {
        return new Complex((RealPart * a.ImagePart + ImagePart * a.RealPart) / (a.ImagePart * a.ImagePart + a.RealPart * a.RealPart), (ImagePart * a.ImagePart + RealPart * a.RealPart) / (a.ImagePart * a.ImagePart + a.RealPart * a.RealPart));
    }
    public boolean equals(Complex o) {
        if (this.ImagePart == o.ImagePart) return true;
        else if (this.RealPart == o.ImagePart) return true;
        else
        return false;
    }
    public String toString() {
        if (ImagePart>0)
        return "Result:" + RealPart +
                "+" + ImagePart+"i" ;
        else if (ImagePart==0)
        return "Result:" + RealPart;
        else
        return "Result:" + RealPart +
                "-" + ImagePart+"i" ;
    }
}
