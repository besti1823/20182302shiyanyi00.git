
public class StringBufferDemo {
    public static void main(String[] args) {
        StringBuffer buffer = new StringBuffer();
        buffer.append('a');
        buffer.append("bcdefghijkl");
        System.out.println(buffer.charAt(1));
        System.out.println(buffer.capacity());
        System.out.println(buffer.length());
        System.out.println(buffer.indexOf("bcdef"));
    }
}


