import junit.framework.TestCase;
import org.junit.Test;
public class ComplexTest extends TestCase {
     Complex a=new Complex(3.0,1.0);
     @Test
     public void testAdd () throws Exception {
          assertEquals("Result:6.0+2.0i",a.ComplexAdd(a).toString());
     }
     @Test
     public void testSub () throws Exception {
          assertEquals("Result:0.0",a.ComplexSub(a).toString());
     }
     @Test
     public void testMulti () throws Exception {
          assertEquals("Result:8.0+6.0i",a.ComplexMulti(a).toString());
     }
     @Test
     public void testDiv () throws Exception {
          assertEquals("Result:0.6+1.0i",a.ComplexDiv(a).toString());
     }
}