package shiyan9;
public class LinkedBinarySearchTreeRun {
    public static void main(String[] args)
    {
        LinkedBinarySearchTree<Integer> tree = new LinkedBinarySearchTree<>();
        tree.addElement(10);
        tree.addElement(18);
        tree.addElement(3);
        tree.addElement(8);
        tree.addElement(12);
        tree.addElement(2);
        tree.addElement(7);
        tree.addElement(3);
        System.out.println("打印树");
        System.out.println(tree.toString());
        System.out.println("\n找最大值" + tree.findMax());
        System.out.println("\n删除最大值" + tree.removeMax());
        System.out.println("\n现在的最大值" + tree.findMax());
        System.out.println("\n能不能找到3: " + tree.contains(2));
        System.out.println("\n能不能找到460: " + tree.contains(2302));
        System.out.println("\n在右子树插入460");
        tree.addElement(2302);
        System.out.println("\n在左子树插入6");
        tree.addElement(6);
        System.out.println("\n删除最小值" + tree.removeMin());
        System.out.println("打印树" + tree.toString());
    }
}