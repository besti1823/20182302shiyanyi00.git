import java.util.Random;
public class Password implements Encryptable{
    private String message;
    private boolean encrypted;
    private int shift;
    private Random generator;
    public Password(String msg)
    {
        message=msg;
        encrypted=false;
        generator=new Random();
        shift =generator.nextInt(10)+5;
    }
    public void encrypt()
    {
        if(!encrypted)
        {
            String masked="";
            for (int index=0;index<message.length();index++)
            masked =masked+(char)(message.charAt(index)+shift*2);
            message =masked;
            encrypted=true;
        }
    }
    public String decrypt()
    {
        if(encrypted)
        {
            String unmasked="";
            for (int index=0;index<message.length();index++)
                unmasked =unmasked+(char)(message.charAt(index)-shift*2);
            message =unmasked;
            encrypted=false;
        }
        return message;
    }
    public boolean isEncrypted()
    {
        return encrypted;
    }
    public String toString()
    {
        return message;
    }
}