class StringToLongException extends Exception{
    StringToLongException(String message)
    {
        super(message);
    }

}