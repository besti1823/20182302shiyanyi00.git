public class Staff {
    private StaffMember[] staffList;
    public Staff()
    {
        staffList =new StaffMember[6];
        staffList[0]=new Executive("Sam","123 Main Line","555-0469","123-45-6789",1);
        staffList[1]=new Employee("Carla","456 Off Line","555-0101","987-65-4321",2);
        staffList[2]=new Employee("Woody","789 Off Rocker","555-0000","010-20-3040",2);
        staffList[3]=new Hourly("Diane","678 Fifth Ave","555-0690","958-47-3625",4);
        staffList[4]=new Volunteer("Norm","987 Suds Blvd.","555-8374");
        staffList[5]=new Volunteer("Cliff","321 Duds Lane","555-7282");
        ((Executive)staffList[0]).awardBonus(6.00);
        ((Hourly)staffList[3]).addHours(40);
    }
    public void holiday(){
        double amount;
                for(int count=0;count<staffList.length;count++)
                {
                    System.out.println(staffList[count]);
                    amount=staffList[count].pay();
                    if(amount==0.0)
                        System.out.println("NO HOLIDAY");
                    else
                        System.out.println("Holiday:"+amount+"days");
                    System.out.println("-------------------");
                }
    }
}
