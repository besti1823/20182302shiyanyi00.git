public class Tree<T> {
    int ok;
    int r=0;
    String str="";
    TreeNode i,j;
    public TreeNode root;
    public int count;
    public Tree(){
        root=null;
    }
    public void creat(int[] a){
        count=a.length;
        for(int b=0;b<count;b++){
            TreeNode temp=  new TreeNode(a[b]);

            if(root==null) root=temp;

            else{
                TreeNode current=root;
                while(true) {
                    if(a[b]>current.Element){
                        if(current.right==null) {current.right=temp;break;}
                        else {
                            current=current.right;
                        }
                    }
                    else if(a[b]<current.Element){
                        if(current.left==null){current.left=temp;break;}
                        else{
                            current=current.left;
                        }
                    }
                }
            }
        }
    }
    public boolean find(int target){

        TreeNode current=root;
        while(current!=null){
            if(current.Element==target) {return true;}
            if(current.Element<target){ current=current.right;}
            else if(current.Element>target ){current=current.left;}
        }
        return false;
    }
    public void del(int target){

        count--;
        int k=0;
        TreeNode current=root;
        while(current!=null){
            if(current.Element==target) {break;}
            if(current.Element<target){
                k=1;
                j=current;
                current=current.right;}
            else if(current.Element>target ){
                k=2;
                i=current;
                current=current.left;}
        }
        int y=0;
        if(current.left==null&&current.right==null){

            if(k==1)
                j.right=null;
            else if (k==2)
                i.left=null;
        }
        else if((current.left!=null&&current.right==null)||(current.left==null&&current.right!=null)){
            if(current.left!=null&&current.right==null) {
                current.setElement(current.left.getElement());
                current.right=current.left.right;
                current.left=current.left.left;}
            else if(current.left==null&&current.right!=null){

                current.setElement(current.right.getElement());
                current.right=current.right.right;
                current.left=current.right.left;
            }
        }
        else  if(current.left!=null&&current.right!=null){
            String r1="";
            r1=dizhong(root);
            String[] r2=r1.split(" ");

            int[] rr=new int[count+1];
            for(int y1=0;y1<=count;y1++){
                rr[y1]= Integer.parseInt(r2[y1]);
            }
            for(int r=0;r<count;r++){
                if(rr[r]==target) {
                    ok=r;
                    break;
                }
            }
            TreeNode current2=root;
            while(current2!=null){
                if(current2.Element==rr[ok-1]) {break;}
                if(current2.Element<rr[ok-1]){
                    k=1;
                    j=current2;
                    current2=current2.right;}
                else if(current2.Element>rr[ok-1] ){
                    k=2;
                    i=current2;
                    current2=current2.left;}
            }
            if(k==1) j.right=null;
            else if(k==2) i.left=null;
            current.setElement(rr[ok-1]);
        }
    }
    public String dizhong(TreeNode temp){
        if(temp==null) return null;
        dizhong(temp.left);
        str+=""+temp.Element+" ";
        r++;
        dizhong(temp.right);
        return str;
    }
    public void toString(TreeNode temp){
        if(temp==null) return;
        toString(temp.left);
        System.out.print(temp.Element+" ");
        toString(temp.right);
    }
    public void insert(int m){
        TreeNode temp=new TreeNode(m);
        if(root==null) root=temp;
        else{
            TreeNode current=root;
            while(true) {
                if(m>current.Element){
                    if(current.right==null) {current.right=temp;break;}
                    else {
                        current=current.right;
                    }
                }
                else if(m<current.Element){
                    if(current.left==null){current.left=temp;break;}
                    else{
                        current=current.left;
                    }
                }
            }
        }
        count++;
    }
}